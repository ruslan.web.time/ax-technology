import defaultTheme from 'tailwindcss/defaultTheme'

export default {
    theme: {
        container: {
            center: true,
            padding: '1rem'
        },
    },
    content: [
        `./assets/**/*.{scss,css,less,svg}`,
    ],
}