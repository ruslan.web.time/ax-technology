// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ['@nuxtjs/tailwindcss'],
  devServer: {
    port: 3001,
  },
  css: ['./assets/styles/styles.scss'],
  devtools: { enabled: false }
})
