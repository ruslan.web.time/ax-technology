export function useParseLinkHeader(header: string) {
    let link = '';
    const linkRegex = /<([^>]+)>;\s*rel="([^"]+)"/g;
    let match;

    while ((match = linkRegex.exec(header)) !== null) {
        const [, url, rel] = match;
        if (rel === 'last') {
            link = url;
        }
    }
    return parseInt(link.split('_page=').at(-1) as string);
}