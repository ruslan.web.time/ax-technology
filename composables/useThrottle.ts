export function useThrottle(callee: Function, timeout: number = 300) {
    let timer:any = null

    return function perform(...args:any) {
        if (timer) return

        timer = setTimeout(() => {
            callee(...args as any)

            clearTimeout(timer)
            timer = null
        }, timeout)
    }
}